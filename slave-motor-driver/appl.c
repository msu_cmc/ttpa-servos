/* 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */

/*
 * appl.c
 *      Motor controller. 
 */

#include <stdint.h>

#include "ttpa.h"
#include "ifs.h"
#include "ifs_types.h"
#include "schedule.h"

#include "appl.h"
#include "pwm.h"
#include "adc.h"

// ADC result
static volatile int16_t adc_ch1_position;
static volatile int16_t adc_ch2_current;

void reg_task(ttpa_taskparam_t param)
{
	//TODO rewrite in fixed point types
	static int16_t err_prev;
	int16_t err;
	int16_t pwm;
	int32_t out;

	err = IFS_TO_INT16(reg_file.ref) - adc_ch1_position; 
	out = (int32_t) (IFS_TO_INT16(reg_file.kp)) * (int32_t) err;
	out += (int32_t) (IFS_TO_INT16(reg_file.kd)) * (int32_t) (err - err_prev);
	err_prev = err;
	
	out >>= 8;
	out += IFS_TO_INT16(reg_file.pwm_corr);
	if (out > INT16_MAX) pwm = INT16_MAX;
	else if (out < INT16_MIN) pwm = INT16_MIN;
	else pwm = out;

	reg_file.pos = INT16_TO_IFS(adc_ch1_position);
	reg_file.current = INT16_TO_IFS(adc_ch2_current);
	reg_file.state = UINT16_TO_IFS(motor_isfault());

	// check ranges; software motion limits
	// TODO set limits from file
	if (adc_ch1_position < 100) pwm = 511;
	else if (adc_ch1_position > 924) pwm = -511;

	reg_file.speed = INT16_TO_IFS(pwm);

	motor_in_pwm(pwm);
}

/*
 * Perform ADC conversation in background.
 */

int adc_bgtask(void) 
{
	static uint8_t adc_channel = 1;
	
	if (!adc_single_check()) return 1;

	if (adc_channel == 1) {
		ATOMIC_BLOCK(ATOMIC_FORCEON) {
			adc_ch1_position = adc_single_get();
		}
		adc_channel = 2;
		adc_select_ch2();
		adc_single_start();
		return 1;
	}

	if (adc_channel == 2) {
		ATOMIC_BLOCK(ATOMIC_FORCEON) {
			adc_ch2_current = adc_single_get();
		}
		adc_channel = 1;
		adc_select_ch1();
		adc_single_start();
		return 1;
	}

}

ADD_BGTASK(adc_bgtask_h, adc_bgtask, 1, (1 << TTPA_STATE_ACTIVE));
		
/*
 * Initialize PWM and ADC modules.
 */

int appl_init(void)
{
	adc_init_single();
	adc_select_ch1();
	adc_single_start();

	motor_init();
	
	return 0;
}

ADD_INITTASK(appl_init_h, appl_init, 9, (1<<TTPA_STATE_UNSYNC));
