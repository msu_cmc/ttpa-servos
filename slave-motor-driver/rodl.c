/*
 * ESE example RODL file for slave3
 */

#include "ifs.h"
#include "ifs_types.h"
#include "ifs_rodl.h"
#include "ttpa_task.h"

#include "appl.h"


// Send data to master
IFS_RODLFILE(0x00, 4, ifs_int_eep)
{
	// Apply control algorithm
	IFS_RE_EXEC(1, IFS_ADDR_I(REG_FN, 1, 0)),
	// Recv data from slaves and place it in RTSEND file
	IFS_RE_SEND_SPDUP(2 + LN_NODE, IFS_ADDR_I(REG_FN, 3, 0), 0, 1),
	// Apply control algorithm
	IFS_RE_EXEC(11, IFS_ADDR_I(REG_FN, 1, 0)),
	IFS_RE_EOR(12),
};

// Receive data from master
IFS_RODLFILE(0x02, 4, ifs_int_eep)
{
	// Apply control algorithm
	IFS_RE_EXEC(1, IFS_ADDR_I(REG_FN, 1, 0)),
	// Recv data from slaves and place it in RTSEND file
	IFS_RE_RECV_SPDUP(2 + LN_NODE, IFS_ADDR_I(REG_FN, 1, 0), 0, 1),
	// Apply control algorithm
	IFS_RE_EXEC(11, IFS_ADDR_I(REG_FN, 1, 0)),
	IFS_RE_EOR(12),
};
