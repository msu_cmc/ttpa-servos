/* 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */

/*
 * appl.h
 *      Motor controller.
 */
#include "ttpa.h"
#include "ifs.h"
#include "ifs_types.h"

#define REG_FN 0x20
#define REG_SEC ifs_int_0

struct reg_file_struct {
	//RECORD 1
	ifs_int16_t ref; ///< reference position
	ifs_int16_t pwm_corr; //<< pwm correction
	//RECORD 2
	ifs_int16_t kp; ///< kp feedback coefficient
	ifs_int16_t kd; ///< kd feedback coefficient
	//RECORD 3
	ifs_int16_t pos;  ///< position sensor data
	ifs_int16_t speed;  ///< speed estimation
	//RECORD 4
	ifs_int16_t current; ///< current sensor data
	ifs_uint8_t state; ///< position controller state
	ifs_uint8_t tmp8; ///< additional data
	//RECORD 5
	ifs_int16_t upper_limit;  ///< position upper limit
	ifs_int16_t lower_limit; ///< position lower limit
};

extern struct reg_file_struct IFS_LOC(REG_SEC) reg_file;

extern void reg_task(ttpa_taskparam_t param);
