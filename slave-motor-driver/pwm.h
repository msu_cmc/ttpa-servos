/** @file pwm.h
@brief Control nterface for pulse-width modulator.
*/

#ifndef PWM_H
#define PWM_H

#include    "bool.h"

#define PWM_TOP 0x3ff

extern void motor_init(void);
extern void motor_ds_pwm(int16_t pwm);
extern void motor_in_pwm(int16_t pwm);
extern BOOL motor_isfault(void);

#endif

