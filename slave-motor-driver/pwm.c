#include <avr/io.h>

#include "config.h"
#include "pwm.h"

/**
@defgroup pwm PWM output module
@{
*/

/**
 * @define PWM_TOP
 * @brief PWM timer TOP value
 */

/** 
 * Init motor control module. 
 * Must be called before any other motor functions. Setup phase-correct 10-bits PWM mode
 */
void motor_init(void) 
{
	// Set output signals
	PWM_ENDDR |= _BV(PWM_EN);
	PWM_DSDDR |= _BV(PWM_DS);
	PWM_IN1DDR |= _BV(PWM_IN1);
	PWM_IN2DDR |= _BV(PWM_IN2);

	// Set input on PC2 with pull up
	PWM_FAULTDDR &= ~_BV(PWM_FAULT);
	PWM_FAULTPORT |= _BV(PWM_FAULT);

	// Phase correct 10-bit PWM. Tt1 = T0clk/1
	// Effective frequency 8 kHz 
	PWM_DS_TOCR = 0x0000; // H-bridge is disabled
	PWM_TCCRA &= ~(_BV(PWM_DS_COM0));
	PWM_TCCRA |= _BV(PWM_DS_COM1) | _BV(PWM_WGM0) | _BV(PWM_WGM1); 
	PWM_TCCRB =  _BV(PWM_CS0);
	// Enable motor
	PWM_ENPORT |= _BV(PWM_EN);
}

/**
 * Set PWM on H-bridge disable pin.
 * @param pwm PWM duty cycle and direction.
 * If @p pwm is positive H-bridge load is coonected in positive direction, negative othewise.
 * Absolute value of @p pwm determines PWM duty cycle. If it is equal to PWM_TOP then H-bridge is alaways
 * enabled. Value 0 coresponds to 0% PWM ratio and H-bridge output is disabled (high impedance state).
 */
void motor_ds_pwm(int16_t pwm) 
{
	if (pwm > 0) {
		// (IN1, IN2) = (LOW, HIGH) 
		PWM_IN1PORT &= ~_BV(PWM_IN1);
		PWM_TCCRA &= ~(_BV(PWM_IN2_COM0) | _BV(PWM_IN2_COM1));
		PWM_IN2PORT |= _BV(PWM_IN2);
	}
	else {
		pwm = -pwm; 
		// (IN1, IN2) = (HIGH, LOW) 
		PWM_IN1PORT |= _BV(PWM_IN1);
		PWM_TCCRA &= ~(_BV(PWM_IN2_COM0) | _BV(PWM_IN2_COM1));
		PWM_IN2PORT &= ~_BV(PWM_IN2);
	}
	PWM_DS_TOCR = (pwm <= PWM_TOP) ? pwm : PWM_TOP;
}

/**
 * Set PWM on H-bridge IN2 pin.
 * @param pwm PWM duty cycle and direction.
 * If @p pwm is positive H-bridge load is coonected in positive direction, negative othewise.
 * Absolute value of @p pwm determines PWM duty cycle. If it is equal to PWM_TOP then H-bridge load
 * is always connected to voltage source. Value 0 coresponds to 0% PWM ratio and load is short circit 
 * thought diode.
 */
void motor_in_pwm(int16_t pwm) 
{
	if (pwm > 0) {
		// (IN1, IN2) = (LOW, HIGH) 
		PWM_IN1PORT &= ~_BV(PWM_IN1);
		// non-inverting PWM mode
		PWM_TCCRA &= ~_BV(PWM_IN2_COM0);
		PWM_TCCRA |= _BV(PWM_IN2_COM1);
		
		PWM_IN2_TOCR = (pwm <= PWM_TOP) ? pwm : PWM_TOP;
		PWM_DS_TOCR = PWM_TOP;
	}
	else if (pwm != 0) {
		pwm = -pwm; 
		// (IN1, IN2) = (HIGH, LOW) 
		PWM_IN1PORT |= _BV(PWM_IN1);
		// inverting PWM mode
		PWM_TCCRA |= _BV(PWM_IN2_COM0) | _BV(PWM_IN2_COM1);

		PWM_IN2_TOCR = (pwm <= PWM_TOP) ? pwm : PWM_TOP;
		PWM_DS_TOCR = PWM_TOP;
	}
	else {
		// disable H-bridge
		PWM_DS_TOCR = 0;
	}
}

/**
 * Check H-bridge fault state.
 * @return If fault flag is set return TRUE.
 * */
BOOL motor_isfault() 
{
	return ! (PWM_FAULTPIN & _BV(PWM_FAULT));
}

/**
@}
*/
