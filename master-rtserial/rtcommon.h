#ifndef  RTCOMMON_H
#define  RTCOMMON_H

#include "config.h"

#define STARTBYTE 'A'
#define TAILBYTE 'B'

#if !defined(RT_BAUDRATE)
#define RT_BAUDRATE 57600
#endif /* RT_BAUDRATE */

#if defined(BAUD_U2X)
#	define RT_UBRR_VALUE ((uint16_t)((CLOCKRATE + 4UL*RT_BAUDRATE)/(8UL*RT_BAUDRATE) - 1UL))
#else
#	define RT_UBRR_VALUE ((uint16_t)((CLOCKRATE + 8UL*RT_BAUDRATE)/(16UL*RT_BAUDRATE) - 1UL))
#endif /* BAUD_U2X */

#define RT_RX_SIG USART2_RX_vect
#define RT_TX_SIG USART2_UDRE_vect
#define RT_UDR UDR2
#define RT_UCSRA UCSR2A
#define RT_UCSRB UCSR2B
#define RT_UCSRC UCSR2C
#define RT_RXC RXC2
#define RT_TXC TXC2
#define RT_UDRE UDRE2
#define RT_FE FE2
#define RT_DOR DOR2 
#define RT_PE UPE2
#define RT_RXCIE RXCIE2
#define RT_TXCIE TXCIE2
#define RT_UDRIE UDRIE2
#define RT_RXEN RXEN2
#define RT_TXEN TXEN2
#define RT_UCSZ0 UCSZ20
#define RT_UCSZ1 UCSZ21
#define RT_UCSZ2 UCSZ22
#define RT_PM0 UPM20
#define RT_PM1 UPM21
#define RT_USBS USBS2
#define RT_UBRR UBRR2

extern volatile uint8_t rt_seq;

#endif  /*RTCOMMON_H*/
