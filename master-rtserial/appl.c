/* Copyright (c) 2004, Christian Troedhandl
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */

/*
 * appl.c
 *      Master RTSerial application file.
 *
 * Setup RTSerial subsytem:
 *     
 *     -# Add RTSEND and RTRECV file for data exchange.
 *     -# Add executable RTSERIAL file. File contains statistics and its execution performs actual data exchange.
 */

#include <stdio.h>

#include "ttpa.h"
#include "ifs.h"
#include "ifs_types.h"
#include "schedule.h"
#include "debug.h"

#include "appl.h"

#define RTSEND_FRAME_LENGTH (1+sizeof(struct rtsend_file_struct))
#define RTRECV_FRAME_LENGTH (1+sizeof(struct rtrecv_file_struct))

#include "rtsend.h"
#include "rtrecv.h"

struct rtsend_file_struct IFS_LOC(RTSEND_SEC) rtsend_file;
struct rtrecv_file_struct IFS_LOC(RTRECV_SEC) rtrecv_file;
struct rtserial_file_struct IFS_LOC(RTSERIAL_SEC) rtserial_file;

IFS_ADDAPPLFILE(RTSEND_FN, &rtsend_file, NULL, IFS_FILELEN(struct rtsend_file_struct), RTSEND_SEC, 066);
IFS_ADDAPPLFILE(RTRECV_FN, &rtrecv_file, NULL, IFS_FILELEN(struct rtrecv_file_struct), RTRECV_SEC, 066);
IFS_ADDAPPLFILE(RTSERIAL_FN, &rtserial_file, rtserial_task, IFS_FILELEN(struct rtserial_file_struct), RTSERIAL_SEC, 077);

void rtserial_task(ttpa_taskparam_t param)
{
	ifs_fd_t fd;
	ifs_addr_t addr;
	char * accum;
	// Use align param as flag: 0x2 -- send flag, 0x1 -- recv flag
	if ((param.align & 0x1) && rtrecv_is_accum_updated()) {
		// Copy data to IFS
		if (ifs_open(&fd, IFS_ADDR(RTRECV_FN, 0x1, 0x0), IFS_REQ(IFS_ACCESS_APPL, IFS_OP_WRITE, 0)) == IFS_ERR_SUCC) {
			accum = rtrecv_get_accum_lock();
			rtserial_file.seq = accum[0];
			ifs_wr_blk(&fd, accum + 1, 0, rtrecv_frame_length() - 1);
			rtrecv_accum_release();
		}
		ifs_close(&fd);
	}
	if (param.align & 0x2) {
		// Copy data from IFS
		if (ifs_open(&fd, IFS_ADDR(RTSEND_FN, 0x1, 0x0), IFS_REQ(IFS_ACCESS_APPL, IFS_OP_READ, 0)) == IFS_ERR_SUCC) {
			accum = rtsend_get_accum_lock();
			accum[0] = rtserial_file.seq;
			ifs_rd_blk(&fd, accum + 1, 0, rtsend_frame_length() - 1);
			rtsend_accum_release();
		}
		ifs_close(&fd);
	}
	// Update statistics
	rtserial_file.recv_state = UINT8_TO_IFS(rtrecv_get_state());
	if (rtserial_file.recv_state != RTRECV_ACTIVE) {
		rtserial_file.recv_fails = UINT8_TO_IFS(IFS_TO_UINT8(rtserial_file.recv_fails) + 1);
	}
	rtserial_file.send_state = UINT8_TO_IFS(rtsend_get_state());
}

int rtserial_init(void) 
{
	rtrecv_init(RTRECV_FRAME_LENGTH);
	rtsend_init(RTSEND_FRAME_LENGTH);
	rtrecv_set_mode(RTRECV_RECV_CONT);
	rtsend_set_mode(RTSEND_SEND_WAIT_ACCUM);
}

ADD_INITTASK(rtserial_init_h, rtserial_init, 9, (1<<TTPA_STATE_UNSYNC));

#if !defined(RTSEND_ISR) || !defined(RTRECV_ISR) 

int rtserial_poll(void)
{
#if !defined(RTRECV_ISR)
	rtrecv_poll();
#endif
#if !defined(RTSEND_ISR)
	rtsend_poll();
#endif 
	return 1;
}

ADD_BGTASK(rtserial_poll_h, rtserial_poll, 1, (1<<TTPA_STATE_ACTIVE));
#endif

int std_init(void)
{

  /* config Uart							*/
  //uart0_init();

  /* connect stdout & stderr with uart0 */

  //printf("Init is finished\n");

  return 0;

}

ADD_INITTASK(std_init_h, std_init, 8, (1<<TTPA_STATE_UNSYNC));
