/**
 * @file rtrecv.c
 * @brief RTRecv module.
 * UART input module: 1 start bit, 1 stop bit, 8 data bit, even parity.
 *
 * Frame format: STARTBYTE BYTE1 BYTE2 ... BYTEn TAILBYTE.
 * 
 * Frame length @p can be defined staticly (macro @ref RTRECV_STATIC_FRAME_LENGTH) or
 * dynamicaly (function @ref rtsend_set_frame_length).
 *
 * Frame body is double buffered. Content of @b acumulator_buffer can be freely read at any time,
 * recived data are stored in @b receive_buffer.
 * Receive buffer is updated by @ref rtrecv_poll function between @ref rtrecv_accum_release and @ref rtrecv_get_accum_lock 
 * invocations.
 */

#ifndef  RTRECV_H
#define  RTRECV_H

#include "config.h"

#include "bool.h"
#include "rtcommon.h"

/**
 * @define RTRECV_FRAME_LENGTH
 * @brief Frame length (accumulator length is equal to frame length).
 * If this macro is not defined by default frame length is equal zero.
 * Use @ref rtrecv_set_frame_length to change it.
 */

/**
 * @define RTRECV_ISR
 * @brief Declare ISR mode.
 * Define this macro to enable ISR mode. @ref rtrev_poll is called by interrupt service routine.
 */

/**
 * @brief RTRecv module states
 */
enum RTRecvState {
	RTRECV_STOPPED = 0, //< receiving is disabled,
	RTRECV_UNSYNC = 1, //< not syncronized with input stream, waiting for valid frame,
	RTRECV_ACTIVE = 2, //< normal operation.
};

/**
 * @brief RTRecv modes of operation
 */
enum RTRecvMode {
	RTRECV_STOP, ///< disable reciving immediatly,
	RTRECV_RECV_CONT , ///< update accumulator content continously.
	RTRECV_RECV_WAIT_ACCUM, ///< do not update accumulator content if it content has not been read yet,
	RTRECV_RECV_STOP, ///< disable receiving after current frame is recevied.
};

typedef uint8_t rtrecv_mode_t; //< RTSend mode of operation
typedef uint8_t rtrecv_state_t; //< RTSend state

/**
 * @fn size_t rtrecv_frame_length(void);
 * @brief Return frame length.
 */
#if !defined(RTRECV_FRAME_LENGTH) 
size_t rtrecv_frame_length(void);
#else
#	define rtrecv_frame_length() RTRECV_FRAME_LENGTH
#endif

/** 
 * @brief Get RTRecv state.
 * @return current state @ref RTRecvState
 */
rtrecv_state_t rtrecv_get_state(void);

/**
 * @brief Change RTRecv mode of operation.
 * @param cmd New mode @ref RTRecvMode
 */
void rtrecv_set_mode(rtrecv_mode_t _mode);

/**
 * @brief Return nonzero if the accumulator contains uread frame.
 */
BOOL rtrecv_is_accum_updated(void);

/**
 * @brief Get pointer to the accumulator and disable accumulator update.
 * Prevent accumulator update receive buffer until @ref rtrecv_accum_release is called.
 * @return pointer to accumulator 
 */
void * rtrecv_get_accum_lock(void);

/**
 * @brief Enable accumulator update.
 */
void rtrecv_accum_release(void);

/**
 * @brief Worker function. Should be called periodicaly if ISR mode is not declared (@ref RTRECV_ISR).
 */
#if !defined(RTRECV_ISR)
void rtrecv_poll(void);
#endif

/**
 * @brief Initialize receiver and set frame length.
 * Do nothing if state is not equal to @a RTRECV_STOPPED.
 * UART mode: 1 start bit, 1 stop bit, 8 data bit, even parity.
 * If macro @ref RTRECV_FRAME_LENGTH is defined, \p frame_length parameter is ignored.
 * @param length frame length in bytes.
 * @return zero if memory allocation failed or device is not ready.
 */ 
BOOL rtrecv_init(size_t length);

/**
 * @brief Deinitialize receiver and free buffer memory.
 * If macro @ref RTRECV_FRAME_LENGTH is defined, memory is not freed.
 * @return zero if device is not ready.
 */ 
BOOL rtrecv_deinit(void);

#endif  /*RTRECV_H*/
